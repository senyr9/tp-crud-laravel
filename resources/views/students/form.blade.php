<div class="form-floating mb-3">
    <input
      value="@isset($student->name){{ $student->name }}@endisset"
      type="text"
      class="form-control" name="name" id="name" 
      placeholder="Votre nom complet :">
    <label for="name">Votre nom complet :</label>
  </div>
  <div class="form-floating mb-3">
      <input 
      value="@isset($student->address){{ $student->address }}@endisset"
      type="text" class="form-control" 
          name="address" id="address"
          placeholder="Votre adresse : ">
      <label for="address">Votre adresse : </label>
  </div>
  <div class="mb-3">
      <label for="image" class="form-label">
          Votre Image
      </label>
      <input 
          class="form-control"
          type="file" name="image" id="image">
  </div>
  <div class="form-floating mb-3">
      <input 
          value="@isset($student->phone){{ $student->phone }}@endisset"
          class="form-control"
          placeholder="Votre numéro de téléphone"
          type="tel" name="phone" id="phone">
      <label for="phone">
          Votre numéro de téléphone : 
      </label>
  </div>
  <div class="form-floating mb-3">
    <input
      value="@isset($student->email){{ $student->email }}@endisset"
      type="email"
      class="form-control" name="email" id="email" 
      placeholder="Votre adresse email : ">
    <label for="email">
      Votre adresse email : 
    </label>
  </div>
  <div class="form-floating mb-3">
      <textarea 
      class="form-control"
      placeholder="Votre biographie : "
      name="bio" id="bio">@isset($student->bio){{ $student->bio }}@endisset</textarea>
      <label for="bio">Votre biographie : </label>
  </div>
  <div class="form-check mb-3">
      <input 
          class="form-check-input"
          value="1" @isset($student->status) checked @endisset 
          type="checkbox" name="status" id="status">
      <label for="status" class="form-check-label">
          Statut de l'étudiant
      </label>
  </div>
  <div class="d-grid gap-2 mb-3">
      <button type="submit" class="btn btn-primary">
          Enregistrer cet étudiant
      </button>
  </div>