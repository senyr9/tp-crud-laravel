@extends("layouts.app")

@section("title", "La liste des étudiants")

@section("content")
    <div class="row">
        <h3>La liste des étudiants</h3>
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Nom complet</th>
                    <th>Adresse</th>
                    <th>Email</th>
                    <th>Téléphone</th>
                    <th>Ations</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($etudiants as $etudiant)
                    <tr>
                        <td>{{ $etudiant->name }}</td>
                        <td>{{ $etudiant->address }}</td>
                        <td>{{ $etudiant->email }}</td>
                        <td>{{ $etudiant->phone }}</td>
                        <td style="display: flex">
                            <a class="btn btn-sm btn-primary" style="margin-right: 5px"
                                href="{{ route("students.show", $etudiant->id) }}">
                                Détail
                            </a>
                            <a class="btn btn-warning btn-sm" style="margin-right: 5px" 
                                href="{{ route("students.edit", $etudiant->id) }}">
                                Modifier
                            </a>
                            <form 
                                action="{{ route('students.destroy', $etudiant->id) }}" 
                                method="post">
                                @csrf
                                @method('DELETE')
                                <button onclick="return confirm('Êtes-vous sûr ?')"
                                    type="submit" class="btn btn-sm btn-danger">
                                    Supprimer
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection