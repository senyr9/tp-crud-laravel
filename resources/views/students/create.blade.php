@extends('layouts.app')

@section("title", "Enregistrement d'un étudiant")

@section("content")
    <div class="row">
        <h3>Enregistrement d'un étudiant</h3>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        <form enctype="multipart/form-data"
            action="{{ route('students.store') }}" method="post">
            @csrf
            @include('students.form')
        </form>
    </div>
@endsection